*Projet en cours de réalisation au FabLab Artilect à Toulouse, groupe Fabtronic*

# Descriptif

Gamelle fermée pour chat (et petit chien ?) au régime avec une ouverture sur identification (collier RFID).

# But du projet

## Contexte

Mon frère a 2 chats dont un avec un problème de reins qui le contraint à une régime à base de croquette €€€. Le second chat n’est pas malade et actuellement ils partagent tous les deux les mêmes croquettes médicales (sans problème pour le chat sain).

Un couple d’amis proche a le même problème, en pire : un chat sain, un chat avec problème rénaux et un chat diabétique. Donc 3 régimes différents, difficiles à gérer.

Le solution sur le marché sont très couteuses (plus de 100€/chat).

## But
Fabrication d’au moins 5 gamelles avec des éléments facile à trouver (gamelle inox, collier), standards (moteur, électronique, puce RFID,...) ou fabricable au FabLab (impression 3D, découpe laser).

* Budget ~100€ pour les 5 gamelles (à ajuster si besoin), hors impression 3D (imprimante à dispo pour le projet)
* Publication d’une documentation, plans et code libre (reproductible sans connaissance trop avancée en électronique, soudure peu nombreuses et faciles)

# Schémas et illustrations

- détection puce RFID => module RDM6300 + antenne sur-mesure + tag RFID EM4100
- détection de présence (éviter une fermeture intempestive pendant le repas) : LED Infrarouge
- motorisation du couvercle (léger) par petit servomoteur ou moteur classique
- microcontrôleur type Arduino (arduino standalone pour réduire les coûts ?) : Arduino Uno chinois (ATmega 168)
- alimentation secteur unique (un bloc secteur pour 1, 2 ou 3 gamelles, probablement 5V USB)
- conception qui protège le mécanisme et l’électronique de la salissure + gamelle extractible (lavage)
- un bouton d’appairage/retrait d’une puce RFID pour plusieurs chats sur une gamelle (ex. : 2 chat sains + 1 malade)

# Documentation externe

* Exemple de solution commerciale : https://www.croquetteland.com/distributeur-automatique-surefeed-avec-puce-d-identification.html#
* Exemple en DIY : http://www.instructables.com/id/RFID-pet-feeder/?ALLSTEPS
